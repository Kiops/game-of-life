Реализация Conway's Game of Life с помощью средств Qt.
Содержит в себе классический режим и модификацию.


Модификация представляет собой клеточный автомат, каждая из клеток которого может быть один из пяти видов юнитов:
0) Пустота
1) Подконтрольный ("отщипывают" клетки от нейтралов и строят из них строения, на клетках, размеченных пользователем)
2) Агрессивный (перемещается по карте в случайных направлениях. Заражает подконтрольные и нейтральные юниты при прикосновении, превращая их в агрессивных. С момента появления или превращения агрессор живет ограниченное количество ходов)
3) Нейтральный (перемещается по карте в случайных направлениях. Может представлять собой формацию из нескольких клеток)
4) Строение (непроходимая неподвижная клетка, которая не может быть заражена. Устанавливается подконтрольным в местах, размеченных игроком, если у подконтрольного имеется в инвентаре клетка нейтрала)
5) Святилище (место появления подконтрольных. Спавнит в своей окрестности подконтрольных, если на карте их численность меньше определенного числа)

Цель игры - избавиться от агрессоров, предотвратив их размножение строительством стен.
Условие поражения - уничтожение агрессорами святилища.