#include "view.h"
#include "controller.h"
#include <QApplication>
#include "grand_view.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    Controller controller;
    controller.start();
    
    return a.exec();
    
}
