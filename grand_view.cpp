#include "grand_view.h"

Grand_view::Grand_view(QWidget* parent) : QWidget(parent)
{   
    view = new View;
    mode = new QPushButton;
    draw = new QRadioButton;
    restart = new QPushButton;
    clear = new QPushButton;
    back = new QPushButton;
    forward = new QPushButton;
    play_pause = new QPushButton;
    greed_l = new QGridLayout;
    horizon_l = new QHBoxLayout;
    
    horizon_l->addWidget(mode);
    horizon_l->addWidget(draw);
    horizon_l->addWidget(clear);
    horizon_l->addWidget(restart);
    horizon_l->addWidget(back);
    horizon_l->addWidget(play_pause);
    horizon_l->addWidget(forward);
    
    connect(view, SIGNAL (place_cell(QPoint, int)), this, SLOT (place_cell_requested(QPoint, int)));
    connect(mode, SIGNAL (pressed()), this, SLOT (button_mode_clicked()));
    connect(clear, SIGNAL (pressed()), this, SLOT (button_clear_clicked()));
    connect(restart, SIGNAL (pressed()), this, SLOT (button_restart_clicked()));
    connect(back, SIGNAL (pressed()), this, SLOT (button_back_clicked()));
    connect(play_pause, SIGNAL(pressed()), this, SLOT (button_play_pause_clicked()));
    connect(forward, SIGNAL (pressed()), this, SLOT (button_forward_clicked()));
    
    greed_l->setMargin(0);
    greed_l->setSpacing(5);
    greed_l->addWidget(view, 0, 0);
    greed_l->addLayout(horizon_l, 1, 0);
    greed_l->setAlignment(Qt::AlignHCenter);
    
    mode->setText("To castle mode");
    draw->setText("Draw");
    clear->setText("Clear");
    restart->setText("Restart");
    back->setText("Back");
    play_pause->setText("Play");
    forward->setText("Forward");
    
    setLayout(greed_l);
    
    is_castle_mode = 0;
}

Grand_view::~Grand_view()
{
    delete view;
    delete back;
    delete forward;
    delete draw;
    delete horizon_l;
    delete greed_l;
}

void Grand_view::disable_buttons(bool trigger)
{
    draw->setDisabled(trigger);
//    restart->setDisabled(trigger);
    clear->setDisabled(trigger);
    back->setDisabled(trigger);
    forward->setDisabled(trigger);
//    play_pause->setDisabled(trigger);
}

void Grand_view::update_view(std::vector <std::vector <double> > apexes)
{
    setUpdatesEnabled(false);
    view->update(apexes);
    update();
    setUpdatesEnabled(true);
}

void Grand_view::button_mode_clicked()
{
    //если игра запущена, останавливаем её
    if(play_pause->text() == "Pause")
        button_play_pause_clicked();
    
    if(mode->text() == "To castle mode")
    {
        mode->setText("To classical mode");
        is_castle_mode = 1;
        
        //выключаем кнопкни
        disable_buttons();
        
    }
    else
    {
        mode->setText("To castle mode");
        is_castle_mode = 0;
        
        //включаем кнопки
        disable_buttons(0);
    }
    emit change_mode(is_castle_mode);
}

void Grand_view::button_restart_clicked()
{
    emit signal_restart();
}

void Grand_view::button_clear_clicked()
{
    emit signal_clear();
}

void Grand_view::button_back_clicked()
{
    //Отключаем мод рисования
    draw->setChecked(0);
    emit change_step(0);
}

void Grand_view::button_forward_clicked()
{
    //Отключаем мод рисования
    draw->setChecked(0);
    emit change_step(1);
}

void Grand_view::button_play_pause_clicked()
{
    //Отключаем мод рисования
    draw->setChecked(0);
    
    //Меняем текст кнопки
    if(play_pause->text() == "Play")
    {
        play_pause->setText("Pause");
        emit play();
    }
    else
    {
        emit play(0);
        play_pause->setText("Play");
    }
}

void Grand_view::place_cell_requested(QPoint cell, int type)
{
    if(is_castle_mode)
    {
        if(type == 1)
            type = 6;
        emit place_cell(cell, type);
    }
    else if(draw->isChecked())
        emit place_cell(cell, type);
}
