#include "controller.h"
#include <math.h>
Controller::Controller()
{
    is_castle_mode = 0;
    
    generation_number = 0;
    draw_mode_was_used = 0;
    
    //настрйока таймера
    timer = new QTimer;
    timer->setInterval(1);
    connect(timer, SIGNAL(timeout()), this, SLOT(change_step()));
    
    //Соединения с визуальной частью
    connect(&window, SIGNAL(signal_clear()), this, SLOT(clear()));
    connect(&window, SIGNAL(signal_restart()), this, SLOT(restart()));
    connect(&window, SIGNAL(play(bool)), this, SLOT(play_or_pause(bool)));
    connect(&window, SIGNAL(change_mode(bool)), this, SLOT(change_mode(bool)));
    connect(&window, SIGNAL(change_step(bool)), this, SLOT(change_step(bool)));
    connect(&window, SIGNAL(place_cell(QPoint, int)), this, SLOT(place_cell(QPoint, int)));
}

void Controller::start()
{
    window.showMaximized();
    paint();
}

void Controller::paint()
{
    std::vector <std::vector <double> >apexes;
    if(is_castle_mode)
        apexes = castle.get_dots();
    else
        apexes = model.get_apexes();
    
    for(int i = 0; i < apexes.size(); i++)
    {
        for(int j= 0; j < 2; j++)
        {
            apexes[i][j] = (apexes[i][j]-50)/50; //todo переправить сотню на корень из произведения длины на ширину поля
        }
    }
    window.update_view(apexes);
}


void Controller::clear()
{
    model.clear();
    draw_mode_was_used = 1;
    paint();
}

void Controller::restart()
{
    if(is_castle_mode)
        castle.restart();
    else
        if(history.size()>0)
        {
            model.set_matrix(history[0]);
            generation_number=0;
        }
    paint();
}

void Controller::change_step()
{
    if(is_castle_mode)
    {
        castle.step();
        paint();
    }
    else
        change_step(true);
}

void Controller::change_step(bool forward = 1)
{
    if(is_castle_mode)
    {
        castle.step();
        paint();
        return;
    }
    if(draw_mode_was_used && generation_number <= history.size())
    {
        if(generation_number == 0)
            history.resize(generation_number);
        else
            history.resize(generation_number-1);
        draw_mode_was_used = 0;
    }
    
    if(generation_number >= history.size())
    {
        history.push_back(model.get_matrix());
    }
    
    //Проверяем в какую сторону выполняется шаг
    if(forward)
        generation_number++;
    else 
        if(generation_number>0)
            generation_number--;
    
    if(generation_number < history.size())
    {
        model.set_matrix(history[generation_number]);
    }
    else
    {
        model.step();
    }
    paint();
}

void Controller::place_cell(QPoint point, int type)
{
    QSize size = window.view->frameSize();
    int x = static_cast<double>(point.x())/size.width()*100 + 0.5;
    int y = abs(static_cast<double>(point.y())/size.height()*100-100) + 0.5;
    if(is_castle_mode)
        castle.add_building_in_progress(std::make_pair(x,y), type);
        
    else
    {
        draw_mode_was_used = 1;
        model.place_life_cell(x, y, type);
    }
    paint();
}

void Controller::play_or_pause(bool trigger)
{
    if(trigger)
        timer->start();
    else
        timer->stop();
}

void Controller::change_mode(bool trigger)
{
    this->is_castle_mode = trigger;
    if(is_castle_mode)
        timer->setInterval(50);
    else
        timer->setInterval(1);
    paint();
}
