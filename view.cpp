#include "view.h"

void View::initializeGL()
{
    glClearColor(0.0,0.0,0.0,0.0);
    glOrtho(-1.0,1.0,-1.0,1.0,1.0,-1.0);
    
}
void View::resizeGL(int w, int h)
{   
    glViewport(0,0,(GLsizei)w,(GLsizei)h);
    size = frameSize();
    size.setWidth(size.height());
    updateGeometry();
}
void View::paintGL()

{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,1,1);
    glPointSize(8);
    glBegin(GL_POINTS);
    for(int i = 0; i < points.size(); i++)
    {
        int type = points[i][3];
        switch (type) {
        case 1:
            glColor3f(1,0.95,0);
            break;
        case 2:
            glColor3f(0.87,0.13,0);
            break;
        case 3:
            glColor3f(0.06,0.48,0.11);
            break;
        case 4:
            glColor3f(0.5,0.05,0.31);
            break;
        case 5:
            glColor3f(0.25, 0.25, 0.25);
            break;
        case 6:
            glColor3f(0.10, 0.10, 0.10);
            break;
        default:
            break;
        }
        glVertex3f(points[i][0], points[i][1], points[i][2]);
    }
    glEnd();
}

//public----------------------------------
View::View(QWidget* parent) : QOpenGLWidget(parent)
{
//    setMinimumWidth(400);
//    setMinimumHeight(400);
//    size.setHeight(400);
//    size.setWidth(400);
}

View::~View()
{
    
}

QSize View::sizeHint() const
{
    return size;
}

void View::update(std::vector <std::vector <double>> new_points)
{
    points = new_points;
    paintGL();
}

void View::mousePressEvent(QMouseEvent* event)
{
    if(event->button() == Qt::RightButton)
        emit place_cell(event->pos(), 0);
    else
        emit place_cell(event->pos(), 1);
}

void View::mouseMoveEvent(QMouseEvent* event)
{
    emit place_cell(event->pos(), 1);
}
