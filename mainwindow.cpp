#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    view = new View;
    ui->gridLayout->addWidget(view);
    
}

MainWindow::MainWindow(const MainWindow &x)
{
    view = new View;
    view = x.view;
}

MainWindow::~MainWindow()
{
    delete view;
    delete ui;
}

void MainWindow::update_view(std::vector <std::vector <double> > apexes)
{
    view->update(apexes);
}


void MainWindow::on_button_back_clicked()
{
    emit change_step(0);
}

void MainWindow::on_button_forward_clicked()
{
    emit change_step(1);
}
