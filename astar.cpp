#include "units_of_castle.h"

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <cmath>
 
void to_standard(int &x, int &y, std::vector<std::vector<bool> > & matrix)
{
    while(x < 0)
        x += matrix.size();
    while(x >= matrix.size())
        x -= matrix.size();
    while(y < 0)
        y += matrix[x].size();
    while(y >= matrix[x].size())
        y -= matrix[x].size();
}
 
struct Point_with_priority{
    int f; /*здесь и далее, f - значение функции f(x) = g(x) + h(x),
                где g(x) - стоимость пути от старта до точки,
                а h(x) - предположительная стоимость пути от точки до финиша.*/
    std::pair <int, int> point;
};
struct lessbypwp{
    bool operator()(const Point_with_priority & left, const Point_with_priority & right){
        return (right.f < left.f);
    }
};
void steps(std::map < std::pair <int, int>, std::pair <int, int> > & from, std::vector < std::pair < int, int > > & ans, std::pair <int, int> point, std::pair <int, int> start) //функция восстановления ответа
{
    if (point == start){
        ans.push_back(point);
    }
    else{
        steps(from, ans, from[point], start);
        ans.push_back(point);
    }
 
}
std::vector < std::pair < int, int > > find_path_by_a_star (std::vector < std::vector <bool> > & matrix, std::pair <int, int> start, std::pair <int, int> finish){
 
    std::set < std::pair <int, int> > open; //множество открытых точек
    std::set < std::pair <int, int> > closed; //множество посещённых точек
    std::map < std::pair <int, int>, int > cost; //расстояние от точки до старта
    std::map < std::pair <int, int>, std::pair <int, int> > from; //для каждой точки хранит предка
    std::stack < std::pair <int, int> > neighbours; //соседние точки, вспомогательный контейнер
    std::vector < std::pair < int, int > > ans; //здесь будет храниться путь от старта до финиша
    std::priority_queue < Point_with_priority, std::vector <Point_with_priority>, lessbypwp> priority_open; //очередь с приоритетом для точек
    Point_with_priority box; //обёртка для того, чтобы поместить новую точку в очередь
    std::pair <int, int> point, neighbour; //point - рассматриваемая точка, neighbour - сосед для неё
 
    to_standard(start.first, start.second, matrix);
    to_standard(finish.first, finish.second, matrix);
    open.insert(start);
    box.point = start;
    box.f = std::min(
                     std::min(std::abs(finish.first - start.first) + std::abs(finish.second - start.second),
                              (int)((matrix.size() - std::abs(finish.first - start.first) + matrix[0].size() - std::abs(finish.second - start.second)))),
                     std::min((int)(std::abs(finish.first - start.first) + matrix[0].size() - std::abs(finish.second - start.second)),
                              (int)(matrix.size() - std::abs(finish.first - start.first) + std::abs(finish.second - start.second)))
                     );
    priority_open.push(box);
    cost[start] = 0;
 
    while (!open.empty()){
        while (closed.count(priority_open.top().point)){ //выкидываю из очереди уже посещённые точки
            priority_open.pop();
        }
        point = priority_open.top().point;
        if (point == finish){
            steps(from, ans, point, start); //если алгоритм дошёл до финища, запускаем функцию восстановления ответа
            return ans;
        }
 
        closed.insert(point);
        open.erase(point);
 
        neighbour = point; //соседей функции закидываем в стек, для будущего анализа
        neighbour.first--;
        to_standard(neighbour.first, neighbour.second, matrix);
        neighbours.push(neighbour);
        neighbour.first += 2;
        to_standard(neighbour.first, neighbour.second, matrix);
        neighbours.push(neighbour);
        neighbour.first--;
        neighbour.second--;
        to_standard(neighbour.first, neighbour.second, matrix);
        neighbours.push(neighbour);
        neighbour.second += 2;
        to_standard(neighbour.first, neighbour.second, matrix);
        neighbours.push(neighbour);
 
        while (!neighbours.empty()){
            if (matrix[neighbours.top().first][neighbours.top().second] == 1){
                closed.insert(neighbours.top());
            }
            else{
                if((!open.count(neighbours.top()) && !closed.count(neighbours.top())) || (cost[point] + 1 < cost[neighbours.top()])){
                    cost[neighbours.top()] = cost[point] + 1;
                    box.f = cost[neighbours.top()] + std::min(
                                                              std::min(std::abs(finish.first - neighbours.top().first) + std::abs(finish.second - neighbours.top().second),
                                                                       (int)((matrix.size() - std::abs(finish.first - neighbours.top().first) + matrix[0].size() - std::abs(finish.second - neighbours.top().second)))),
                                                              std::min((int)(std::abs(finish.first - neighbours.top().first) + matrix[0].size() - std::abs(finish.second - neighbours.top().second)),
                                                                       (int)(matrix.size() - std::abs(finish.first - neighbours.top().first) + std::abs(finish.second - neighbours.top().second)))
                                                              );
                    box.point = neighbours.top();
                    priority_open.push(box);
                    from[neighbours.top()] = point;
                }
                if ((!open.count(neighbours.top()) && !closed.count(neighbours.top()))){
                    open.insert(neighbours.top());
                }
            }
            neighbours.pop();
        }
    }
    point.first = -1; //если алгоритм вышел из цикла, возвращаем ошибку или невозможное значение
    point.second = -1;
    ans.push_back(point);
    return ans;
}
