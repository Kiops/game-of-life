#pragma once

#include <utility>
#include <vector>
#include <set>
#include <string>
#include <list>

const unsigned int lifetime_of_agressive = 200;
const unsigned int minimal_controls_amount = 4;
const unsigned int starting_controls_amount = 4; //не может быть больше восьми
const unsigned int minimal_neutrals_amount = 10;
const unsigned int starting_neutrals_amount = 100;
const unsigned int starting_agressives_amount = 10;

class Unit;

using Matrix = std::vector<std::vector<Unit *>>;
using Coords = std::vector<std::pair<int, int>>;
using Bool_matrix = std::vector<std::vector<bool>>;

class Visitor;


class Unit //базовый класс
{
protected:
    Coords coords; //координаты, занмимаемые данным юнитом
    std::vector<std::vector <Unit *>> * matrix;
    std::set<Unit*>* units;

    void die_or_write(); //записывает в матрицу адрес данного юнита
    //или вызывает dissapear, если место занято
    void rand_step(); //делает шаг в случайном направлении
    void get_to_new_coords(Coords new_coords); //передвигается на новые
    //координаты, освобождая старые
    void translate_direction_in_coords(const int& direction, int& dx, int& dy); //выбирает сдвиг по координатам в соответствии с направлением
    Coords find_free_random_coords(bool two_and_more_coords = 0);
    void make_compicated_unit(Coords&  constructing_coords); //"приклеивает" к нескольким из имеющихся координат ещё несколько
    
    //методы для нахождения пути
    std::pair<int, int> find_closest(std::pair <int, int> start, std::string needed_type, Coords selected_destinations);
    Coords find_path_to_closest(std::pair<int,int> my_position, std::string aim_type, bool avoid_agressive); //находит путь от my_position к ближайшей незанятой клетке типа type
    Bool_matrix matrix_to_bool(bool avoid_agressive); //возвращает двоичную проекцию матрицы //TODO (?) убрать метод из класса, принимая матрицу по адресу (?)
    Coords find_path(std::pair<int, int> start, std::pair<int,int> finish, bool avoid_agressive);
public:
    bool operator < (Unit& x);

    Unit(Matrix* matrix, std::set<Unit*>* units);
    
    Coords get_coords();
    void to_standard(int& x, int& y); //изменяет x и y в соответствии с плоскостью сферы
    bool free_space(Coords checking_coords); //проверяет, свободно ли место
    //по данным координатам
    virtual void lose(int x, int y);
    
    virtual void step();
    virtual void accept(Visitor &v) = 0;
    
    void disappear(); //зачищает место в матрице по своим координатам
    void write_in_matrix(); //записывает адрес данного объекта в матрицу
    //по координатам, записанным в coords
};




class Control : public Unit //управляемые юниты
{   
    bool carrying; //несет ли строительный блок
    bool en_route; //находится ли в пути
    std::pair <int, int> destination; 
    
public:
    static int number_of_free_jobs;
    static Coords selected_destinations;
    static std::set <std::pair<int,int>> buildings_in_progress;
    explicit Control(Matrix* matrix, std::set<Unit*>* units, int x, int y);
    
    void accept(Visitor &v) override;
    void step() override;
};

class Agressive : public Unit //агрессивные
{
    int lifetime_left;
    void infect(); //заражает всех юнитов в радиусе действия
public:
    explicit Agressive(Matrix* matrix, std::set<Unit*>* units);
    explicit Agressive(Matrix* matrix, std::set<Unit*>* units, Coords spawn_coords);
    
    void accept(Visitor &v) override;
    void step() override;
};

class Neutral : public Unit //нейтралы
{
public:
    explicit Neutral(Matrix* matrix, std::set<Unit*>* units);
    
    void accept(Visitor &v) override;
    void lose(int x, int y) override; //теряет клетку по координатам x, y. Может привести к удалению объекта, если других клеток у него не осталось
    void step() override;
};

class Grail : public Unit //святыня
{
public:
    explicit Grail(Matrix* matrix, std::set <Unit*>* units);
    
    void accept(Visitor& v) override;
    void step() override; //принимает в себя вектор живых управляемых, если их недостаточно - спавнит ещё
    void spawn(); //спавнит одного подконтрольного
};

class Building : public Unit //строения (построенные и только назначенные)
{
public:
//    explicit Building(Matrix* matrix, std::set<Unit*>* units);
    explicit Building(Matrix* matrix, std::set<Unit*>* units, int x, int y);
    
    void accept(Visitor &v) override;
};

//Визитёры~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Visitor
{
public:
    virtual void visit(Control& ref) = 0;
    virtual void visit(Agressive& ref) = 0;
    virtual void visit(Neutral& ref) = 0;
    virtual void visit(Grail& ref) = 0;
    virtual void visit(Building& ref) = 0;  
};

class GetType : public Visitor
{
public: 
    std::string value;
    int priority; //приоритет типа
    void visit(Control& ref) override;
    void visit(Agressive& ref) override;
    void visit(Neutral& ref) override;
    void visit(Grail& ref) override;
    void visit(Building& ref) override;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Coords find_path_by_a_star (std::vector < std::vector <bool> > & matrix, std::pair <int, int> start, std::pair <int, int> finish);