#pragma once
#include <vector>

using Matrix_int = std::vector<std::vector<int>>;

class Model
{
    std::vector <std::vector <int>> matrix;
    void to_standard(int& x, int& y); //подогнать координаты под сферическое простанство 
    int should_spawn(int x, int y); //0 - удалить, 1 - заспавнить жизнь нужного вида
public:
    Model();
    Model(int x, int y);
    Model(const Model &new_model);
    
    void step();
    Matrix_int get_matrix();
    std::vector <std::vector <double> > get_apexes();
    void set_matrix(const Matrix_int& new_matrix);
    void place_life_cell(int x, int y, int kind);
    void clear(); //обнуляет все клетки
    };
