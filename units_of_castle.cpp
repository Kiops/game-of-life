#include "units_of_castle.h"

#include <algorithm>
#include <ctime>

std::set <std::pair<int,int>> Control::buildings_in_progress;
Coords Control::selected_destinations;
int Control::number_of_free_jobs;

template <class T>
bool in_vector(const std::vector<T>& vect, const T& checking_element)
{
    for(auto it = vect.begin(); it != vect.end(); it++)
    {
        if(*it == checking_element)
            return 1;
    }
    return 0;
}

Coords Unit::find_path(std::pair<int, int> start, std::pair<int,int> finish, bool avoid_agressive)
{
    Coords result;  
    
    //создаём двоичную матрицу и исправляем её для использования aStar
    Bool_matrix bool_matrix = matrix_to_bool(avoid_agressive);
    bool_matrix[start.first][start.second] = 0;
    bool_matrix[finish.first][finish.second] = 0;
    
    result = find_path_by_a_star(bool_matrix, start, finish);
    return result;
}

struct Delusional_cell
{
    bool needed_type;
    bool baricade;
    bool visited;
    
    Delusional_cell(bool needed_type = 0, bool baricade = 0, bool visited = 0)
    {
        this->needed_type = needed_type;
        this->baricade = baricade;
        this->visited = visited;
    }
};

struct Delusional_matrix
{
    std::string needed_type;
    std::vector<std::vector <Delusional_cell>> delusional_matrix;
    Delusional_matrix(Matrix * matrix, std::string needed_type)
    {
        this->needed_type = needed_type;
        for(int i = 0; i < matrix->size(); i++)
        {
            std::vector <Delusional_cell> transit;
            for(int j = 0; j < (*matrix)[i].size(); j++)
            {
                if((*matrix)[i][j] == nullptr)
                    transit.push_back(Delusional_cell());
                else
                {
                    GetType visitor;
                    (*matrix)[i][j]->accept(visitor);
                    {
                        if(visitor.value == this->needed_type)
                            transit.push_back(Delusional_cell(1, 0));
                        else
                            transit.push_back(Delusional_cell(0, 1));
                    }
                }
            }
            delusional_matrix.push_back(transit);
        }
        if(needed_type == "Building in progress")
        {
            for (auto it = Control::buildings_in_progress.begin(); it != Control::buildings_in_progress.end(); it++)
            {
                int x = it->first;
                int y = it->second;
                delusional_matrix[x][y].needed_type = 1;
            }
        }
    }
    void mark_selected(Coords selected_destinations)
    {
        for(int i = 0; i < selected_destinations.size(); i++)
        {
            int x = selected_destinations[i].first;
            int y = selected_destinations[i].second;
            delusional_matrix[x][y].visited = 1;
        }
    }
};

std::pair<int, int> Unit::find_closest(std::pair <int, int> start, std::string needed_type, Coords selected_destinations)
{
    //Защита от тормозов
    if(needed_type != "Building in progress")
    {
        GetType kind_visitor;
        bool trigger = 0;
        for(auto it = units->begin(); it != units->end(); it++)
        {
            (*it)->accept(kind_visitor);
            if(kind_visitor.value == needed_type)
            {
                trigger = 1;
                break;
            }
        }
        if(!trigger)
            return std::make_pair(-1,-1);        
    }
    else if(needed_type == "Building in progress" && Control::buildings_in_progress.empty())
        return std::make_pair(-1,-1);
    
    
    Delusional_matrix dmatrix (matrix, needed_type);
    dmatrix.mark_selected(selected_destinations);
    std::set <std::pair<int, int>> last_visited_coords;
    last_visited_coords.insert(start);
    int counter_of_steps = 0;
    while(!last_visited_coords.empty())
    {
        Delusional_matrix copy_dmatrix = dmatrix;
        std::set <std::pair<int, int>>  next_last_visited_coords;
        counter_of_steps++;
        for(auto it = last_visited_coords.begin(); it !=  last_visited_coords.end(); it++)
        {            
            int pending_x = it->first;
            int pending_y = it->second;
            for(int dx = -1; dx < 2; dx++)
            {
                for(int dy = -1; dy < 2; dy++)
                {
                    //игнорируем возможность рассматривать угловые клетки окрестности и её центр
                    if(dx*dx == dy*dy)
                        continue;
                    int x = pending_x + dx;
                    int y = pending_y + dy;
                    to_standard(x, y);
                    //игнорируем посещенные клетки
                    if(dmatrix.delusional_matrix[x][y].visited)
                        continue;
                    //игнорируем барикады
                    if(dmatrix.delusional_matrix[x][y].baricade)
                        continue;
                    //реагируем на тип
                    if(dmatrix.delusional_matrix[x][y].needed_type)
                        return std::make_pair(x, y);
                    else
                    {
                        copy_dmatrix.delusional_matrix[x][y].visited = 1;
                        next_last_visited_coords.insert(std::make_pair(x, y));
                    }
                }
            }
        }
        last_visited_coords = next_last_visited_coords;
        next_last_visited_coords.clear();
        dmatrix = copy_dmatrix;
    }
    return std::make_pair(-1,-1);
}

Coords Unit::find_path_to_closest(std::pair<int, int> my_position, std::string aim_type, bool avoid_agressive)
{   
    std::pair<int, int> aim = find_closest(my_position, aim_type, Control::selected_destinations);
    if(aim.first == -1)
    {
        Coords blank;
        return blank;
    }
    return find_path(my_position, aim, avoid_agressive);
}

Bool_matrix Unit::matrix_to_bool(bool avoid_agressive)
{
    //создали матрицу, заполнили нулями
    std::vector < std::vector <bool>> bool_matrix;
    for(int i = 0; i < matrix->size(); i++)
    {
        std::vector <bool> transit;
        for(int j = 0; j < (*matrix)[i].size(); j++)
        {
            transit.push_back(0);
        }
        bool_matrix.push_back(transit);
    }
    
    //отметили единицами непроходимые клетки
    for(auto it = units->begin(); it != units->end(); it++)
    {
        GetType visitor;
        (*it)->accept(visitor);
        for(int n = 0; n < (*it)->get_coords().size(); n++)
        {
            int x = (*it)->get_coords()[n].first;
            int y = (*it)->get_coords()[n].second;
            
            to_standard(x, y);
            
            //если клетка агрессивная и мы её избегаем, то отмечаем непроходимой её окрестность
            if(visitor.value == "Agressive" && avoid_agressive)
            {
                for(int dx = -1; dx < 2; dx++)
                    for(int dy = -1; dy < 2; dy++)
                    {
                        int nx = x + dx;
                        int ny = y + dy;
                        to_standard(nx, ny);
                        bool_matrix[nx][ny] = 1;
                    }
            }
            else
            {
                bool_matrix[x][y] = 1;
            }
        }
    }
    return bool_matrix;
}

Coords Unit::find_free_random_coords(bool two_and_more_coords)
{   
    Coords possible_coords;
    int x = rand() % 99;
    int y = rand() % 99;
    possible_coords.push_back(std::make_pair(x, y));
    
    if(two_and_more_coords)
        make_compicated_unit(possible_coords);
    
    for(int i = 0; i < 100; i++)
    {
        //для каждой x-координаты
        for(int n = 0; n < possible_coords.size(); n++)
            possible_coords[n].first++;
        for(int j = 0; j < 100; j++)
        {
            //для каждой y-координаты
            for(int n = 0; n < possible_coords.size(); n++)
                possible_coords[n].second++;
            
            //проверяем доступность места, если доступно - завершаем
            for(int n = 0; n < possible_coords.size(); n++)
            {
                to_standard(possible_coords[n].first, possible_coords[n].second);
            }
            if(free_space(possible_coords))
            {
                x = 100; //выполняем условие выхода из цикла по x
                break; //выходим из цикла по y
            }
        }
    }
    return possible_coords;
}

void Unit::make_compicated_unit(Coords&  constructing_coords)
{
    if(constructing_coords.empty())
        return;
    int number_of_additional_dots = rand() % 4;
    for(int i = 0; i < number_of_additional_dots; i++)
    {
        for(int j = 0; j < constructing_coords.size(); j++)
        {
            //выбираем случайную координату из имеющихся
            int aim = rand() % constructing_coords.size();
            int direction = rand() % 4;
            int dx = 0;
            int dy = 0;
            
            for(int k = 0; k < 4; k++)
            {
                //вычисляем новые координаты по сдвигу от цели
                translate_direction_in_coords(direction, dx, dy);
                int x = constructing_coords[aim].first + dx;
                int y = constructing_coords[aim].second + dy;
                
                to_standard(x, y);
                
                //проверяем, есть ли уже такая точка в составе фигуры
                auto it = std::find(constructing_coords.begin(), constructing_coords.end(), std::make_pair(x, y));
                if(it == constructing_coords.end())
                {
                    //добавляем точку, выходим из цикла, переходим к созданию очередной
                    constructing_coords.push_back(std::make_pair(x, y));
                    j = constructing_coords.size();
                    break;
                }
                
                //изменяем направление
                if(direction == 3)
                    direction = 0;
                else direction++;
            }
        }
    }
}

void Unit::translate_direction_in_coords(const int& direction, int& x, int& y)
{
    x = 0;
    y = 0;
    
    if(direction == 0)
        x = -1;
    else if(direction == 1)
        x = 1;
    else if(direction == 2)
        y = -1;
    else
        y = 1;
}

void Unit::get_to_new_coords(Coords new_coords)
{
    for(int i = 0; i < coords.size(); i++)
    {
        int x = coords[i].first;
        int y = coords[i].second;
        to_standard(x, y);
        if((*matrix)[x][y]==this)
            (*matrix)[x][y] = nullptr;
    }
    coords = new_coords;
    write_in_matrix();
}

void Unit::rand_step()
{
    Coords next_coords = coords;
    int direction = rand() % 4; //4 - количество возможных направлений
    int x = 0;
    int y = 0;
    for(int i = 0; i < 4; i++) //4 - количество возожных направлений
    {
        translate_direction_in_coords(direction, x, y);
        
        //изменяем каждую координату в соответсвии со сдвигом
        for(int n = 0; n < next_coords.size(); n++)
        {
            next_coords[n].first += x;
            next_coords[n].second += y;
            to_standard(next_coords[n].first, next_coords[n].second);
        }
        //если направление подходит,
        //то завершаем цикл по i
        if(free_space(next_coords))
            break;
        //в противном случае изменяем направление
        else
        {
            if(direction==3)
                direction = 0;
            else direction++;
        }
    }
    get_to_new_coords(next_coords);
}

void Unit::die_or_write()
{
    if(free_space(coords))
        write_in_matrix();
    else
        disappear();
}

bool Unit::operator < (Unit& x)
{
    GetType visitor;
    accept(visitor);
    int my_prior = visitor.priority;
    x.accept(visitor);
    int x_prior = visitor.priority;
    return my_prior < x_prior;
}

void Unit::disappear()
{
    for (int i = 0; i < coords.size(); i++)
    {
        int x = coords[i].first;
        int y = coords[i].second;
        if((*matrix)[x][y] == this)
            (*matrix)[x][y]=nullptr;
    }
    units->erase(this);
    delete this;
}

void Unit::write_in_matrix()
{
    //перестраховываемся
    for(int i = 0; i < coords.size(); i++)
        to_standard(coords[i].first, coords[i].second);
    
    //перестраховываемся
    for(int i = 0; i < matrix->size(); i++)
        for(int j = 0; j < (*matrix)[i].size(); j++)
            if((*matrix)[i][j] == this)
                (*matrix)[i][j] = nullptr;
    
    for(int i = 0; i < coords.size(); i++)
    {
        int x = coords[i].first;
        int y = coords[i].second;
        (*matrix)[x][y]=this;
    }
}

Coords Unit::get_coords()
{
    return coords;
}

void Unit::to_standard(int &x, int &y)
{
    while(x < 0)
        x += (*matrix).size();
    while(x >= (*matrix).size())
        x -= (*matrix).size();
    while(y < 0)
        y += (*matrix)[x].size();
    while(y >= (*matrix)[x].size())
        y -= (*matrix)[x].size();
}

bool Unit::free_space(Coords checking_coords)
{
    for(int i = 0; i < checking_coords.size(); i++)
    {
        int x = checking_coords[i].first;
        int y = checking_coords[i].second;
        to_standard(x, y);
        Unit * adress = (*matrix)[x][y];
        if(adress != nullptr && adress != this)
            return 0;
    }
    return 1;
}

void Unit::lose(int x, int y)
{
    //здес должно быть пусто
}

//Конструкторы~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Unit::Unit(Matrix* matrix, std::set<Unit*>* units)
{
    this->matrix = matrix;
    this->units = units;
}

Control::Control(Matrix* matrix, std::set<Unit*>* units, int x, int y) : Unit(matrix, units)
{
    carrying = 0;
    coords.push_back(std::make_pair(x,y));
    die_or_write();
}

Agressive::Agressive(Matrix* matrix, std::set<Unit*>* units) 
    : Unit(matrix, units)
{
    lifetime_left = lifetime_of_agressive;
    coords = find_free_random_coords(0);
    die_or_write();
}

Agressive::Agressive(Matrix *matrix, std::set<Unit *> *units, Coords spawn_coords)
    : Unit(matrix, units)
{
    lifetime_left = lifetime_of_agressive;
    //на всякий случай приводим координаты к стандарту
    for(int i = 0; i < spawn_coords.size(); i++)
        to_standard(spawn_coords[i].first, spawn_coords[i].second);
    
    coords = spawn_coords;
    die_or_write();
}

Neutral::Neutral(Matrix* matrix, std::set<Unit*>* units) : Unit(matrix, units)
{
    coords = find_free_random_coords(1);
    die_or_write();
}

Grail::Grail(Matrix* matrix, std::set <Unit*>* units) : Unit(matrix, units)
{
    this->units = units; //запомнили адрес на множество подконтрольных
    srand(time(0));
    coords.push_back(std::make_pair(49,49)); //задали координаты святыни
    write_in_matrix(); //отобразили её в матрице
    
    //Заспавнили первых подконтрольных
    for (int i = 0; i < starting_controls_amount; i++)
    {
        spawn();
    }
}

Building::Building(Matrix* matrix, std::set<Unit*>* units, int x, int y) : Unit(matrix, units)
{
    coords.push_back(std::make_pair(x, y));
    die_or_write();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//Шаги~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Unit::step()
{
    //здесь дожно быть пусто
}

void Control::step()
{
    if(carrying)
    {
        if(number_of_free_jobs < 1)
            return;
        Coords path;
        path = find_path_to_closest(coords[0], "Building in progress", 1);
        if(path.size()<2)
            return;
        number_of_free_jobs--;
        if(path.size() == 2)
        {
            int x = path.back().first;
            int y = path.back().second;
            if((*matrix)[x][y] != nullptr)
                (*matrix)[x][y]->disappear();
            Building* building = new Building(matrix, units, x, y);
            carrying = 0;
            units->insert(building);
            buildings_in_progress.erase(std::make_pair(x,y));
        }
        else
        {
            selected_destinations.push_back(path.back());
            Coords next_coords;
            next_coords.push_back(path[1]);
            get_to_new_coords(next_coords);
        }
    }
    else
    {
        Coords path;
        path = find_path_to_closest(coords[0], "Neutral", 1);
        if(path.size() < 2)
            return;
        if(path.size() == 2)
        {
            int x = path.back().first;
            int y = path.back().second;
            ((*matrix)[x][y])->lose(x, y);//откусить
            carrying = 1;
        }
        else
        {
            selected_destinations.push_back(path.back());
            Coords next_coords;
            next_coords.push_back(path[1]);
            get_to_new_coords(next_coords);
        }
    }
}

void Agressive::step()
{
    lifetime_left--;
    infect();
    rand_step();
    infect();
    if(lifetime_left == 0)
    {
        disappear();
    }
}

void Neutral::step()
{
    rand_step();
}

void Grail::step()
{
    int counter = 0;
    for(std::set<Unit *>::iterator it = units->begin(); it != units->end(); it++)
    {
        GetType visitor;
        (*it)->accept(visitor);
        if(visitor.value == "Control")
            counter++;
    }
    
    if(counter < minimal_controls_amount)
    {
        spawn();
    }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void Grail::spawn()
{    
    //координа святилища
    int x = coords[0].first;
    int y = coords[0].second;
    
    //ищет все свободные клетки в окрестности святилища
    Coords free_cells;
    for(int i = -1; i < 2; i++)
    {
        for(int j = -1; j < 2; j++)
        {
            if(i == 0 && j == 0)
                continue;
            if((*matrix)[x+i][y+j] == nullptr)
            {
                free_cells.push_back(std::make_pair(x+i, y+j));
            }
        }
    }
    
    //из свободных клеток выбирает случайную и спавнит там подконтрольного
    if(free_cells.size() == 0)
        return;
    int place_id = rand() % free_cells.size();
    Control* ctrl = new Control(matrix, units, free_cells[place_id].first, free_cells[place_id].second);
    units->insert(ctrl);
}

void Agressive::infect()
{
    //для каждой клетки агрессора
    for(int n = 0; n < coords.size(); n++)
    {
        //проверить все ближайшие клетки
        for(int dx = -1; dx < 2; dx++)
            for(int dy = -1; dy < 2; dy ++)
            {
                if(dx == 0 && dy == 0)
                    continue;
                
                int x = coords[n].first + dx;
                int y = coords[n].second + dy;
                to_standard(x, y);
                
                Unit * adress = (*matrix)[x][y];
                
                if (adress == nullptr)
                    continue;
                
                GetType visitor;
                adress->accept(visitor);
                if(visitor.value != "Agressive" && visitor.value != "Building")
                {
                    Coords victim_coords = adress->get_coords();
                    adress->disappear();
                    units->insert(new Agressive(matrix, units, victim_coords));
                }
            }
    }
}

void Neutral::lose(int x, int y)
{
    coords.erase(remove(coords.begin(), coords.end(), std::make_pair(x, y)));
    write_in_matrix();
    if(coords.empty())
        disappear();
}

//Методы для визитёров~~~~~~~~~~~~
void Control::accept(Visitor &v)
{
    v.visit(*this);
}

void Agressive::accept(Visitor &v)
{
    v.visit(*this);
}

void Neutral::accept(Visitor &v)
{
    v.visit(*this);
}

void Grail::accept(Visitor &v)
{
    v.visit(*this);
}

void Building::accept(Visitor &v)
{
    v.visit(*this);
}

void GetType::visit(Control& ref)
{
    value = "Control";
    priority = 0;
}

void GetType::visit(Agressive& ref)
{
    value = "Agressive";
    priority = 1;
}

void GetType::visit(Neutral& ref)
{
    value = "Neutral";
    priority = 2;
}

void GetType::visit(Grail& ref)
{
    value = "Grail";
    priority = 3;
}

void GetType::visit(Building& ref)
{
    value = "Building";
    priority = 4;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

