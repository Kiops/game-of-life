#pragma once

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QRadioButton>
#include <view.h>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QTimer>

class Grand_view : public QWidget
{
    Q_OBJECT
    
    //Виджет OGL и лейауты
    QGridLayout* greed_l;
    QHBoxLayout* horizon_l;
    
    //кнопки
    QPushButton* back;
    QPushButton* forward;
    QRadioButton* draw;
    QPushButton* mode;
    QPushButton* play_pause;
    QPushButton* restart;
    QPushButton* clear;
    
    bool is_castle_mode;
public:
    View* view;
    
    
    explicit Grand_view(QWidget* parent = 0);
    ~Grand_view();
    
    void disable_buttons(bool trigger = 1);
    
public slots:
    void update_view(std::vector <std::vector <double> > apexes);

private slots:
    
    void button_mode_clicked();
    void button_restart_clicked();
    void button_clear_clicked();
    void button_back_clicked();
    void button_forward_clicked();
    void button_play_pause_clicked();
    void place_cell_requested(QPoint, int);
    
signals:
    void change_mode(bool is_castle_mode);
    void signal_restart();
    void signal_clear();
    void change_step(bool forward);
    void play(bool start = 1);
    void place_cell(QPoint cell, int type);
};
    
