#include "model.h"


void Model::to_standard(int &x, int &y)
{
    while(x < 0)
        x += matrix.size();
    while(x >= matrix.size())
        x -= matrix.size();
    while(y < 0)
        y += matrix[x].size();
    while(y >= matrix[x].size())
        y -= matrix[x].size();
}

int Model::should_spawn(int x, int y)
{
    int counter = 0;
    for(int i = -1; i <=1; i++)
    {
        for(int j = -1; j <= 1; j++)
        {
            if(i!= 0 || j!=0)
            {
                int sx = x + i;
                int sy = y + j;
                to_standard(sx, sy);
                if(matrix[sx][sy]!=0)
                    counter++;
            }
        }
    }
    if(counter == 3)
        return 1;
    if(counter == 2 && matrix[x][y]!=0)
        return 1;
    return 0;
}

//public--------------------------------------------------------

Model::Model()
{
    for(int i = 0; i < 100; i++)
    {
        std::vector <int> line;
        for(int j = 0; j < 100; j++)
            line.push_back(0);
        line.shrink_to_fit();
        matrix.push_back(line);
    }
    matrix.shrink_to_fit();
}

Model::Model(int x, int y)
{
    
    for(int i = 0; i < x; i++)
    {
        std::vector <int> line;
        for(int j = 0; j < y; j++)
            line.push_back(0);
        line.shrink_to_fit();
        matrix.push_back(line);
    }
}

Model::Model(const Model &new_model)
{
    *this = new_model;
}

std::vector < std::vector <int> > Model::get_matrix()
{
    return matrix;
}

void Model::step()
{
    std::vector < std::vector <int> > transit_matrix = matrix;
    for(int x = 0; x < matrix.size(); x++)
    {
        for(int y = 0; y < matrix[x].size(); y++)
        {
            if(should_spawn(x, y))
            {
                transit_matrix[x][y]=1;
            }
            else
                transit_matrix[x][y]=0;
        }
    }
    matrix = transit_matrix;
}

void Model::set_matrix(const Matrix_int& new_matrix)
{
    matrix = new_matrix;
}

std::vector <std::vector <double> > Model::get_apexes()
{
    std::vector <std::vector <double> > result;
    for(int i = 0; i < matrix.size(); i++)
        for(int j = 0; j < matrix[i].size(); j++)
        {
            if(matrix[i][j]!=0)
            {
                std::vector <double> point;
                point.push_back(i);
                point.push_back(j);
                point.push_back(0);
                point.push_back(matrix[i][j]);
                result.push_back(point);
            }
        }
    return result;
}

void Model::place_life_cell(int x, int y, int kind)
{
    if(x < matrix.size() && y < matrix[y].size())
    {
        matrix[x][y] = kind;
    }
}

void Model::clear()
{
    for(int i = 0; i < matrix.size(); i++)
    {
        for(int j = 0; j < matrix[i].size(); j++)
        {
            matrix[i][j]=0;
        }
    }
}
