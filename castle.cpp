#include "castle.h"

void Castle::create_matrix()
{
    //Заполняем матрицу нуль-указателями
    for (int i = 0; i < 100; i++)
    {
        std::vector<Unit*> transit;
        for (int j = 0; j < 100; j++)
        {
            transit.push_back(nullptr);
        }
        transit.shrink_to_fit();
        matrix.push_back(transit);
    }
    matrix.shrink_to_fit();
}

void Castle::set_starting_state()
{
    //устанавливаем святилище
    //святилище спавнит несколько подконтрольных
    //в момент появления
    units.insert(new Grail(&matrix, &units));
    
    //здесь нужно спавнить несколько агрессоров
    for(int i = 0; i < starting_agressives_amount; i++)
        units.insert(new Agressive(&matrix, &units));
    
    //здесь нужно спавнить несколько мирных
    for(int i = 0; i < starting_neutrals_amount; i++)
        units.insert(new Neutral(&matrix, &units));
}

Castle::Castle()
{
    create_matrix();
    set_starting_state();    
}

Castle::~Castle()
{
    Control::buildings_in_progress.clear();
    for(auto it = units.begin(); it != units.end(); it++)
    {
        Unit* unit_adress= *it;
        if(unit_adress != nullptr)
            delete unit_adress;
    }
    matrix.clear();
    units.clear();
}

void Castle::restart()
{
    //подчищяем прошлую игру
    for(auto it = units.begin(); it != units.end(); it++)
        (*it)->disappear();
    Control::buildings_in_progress.clear();
    
    //устанавливаем новую
    set_starting_state();
}

void Castle::step()
{
    
    Control::number_of_free_jobs = Control::buildings_in_progress.size();
    Control::selected_destinations.clear();
    
    //каждый юнит делает шаг
    for(auto it = units.begin(); it != units.end(); it++)
    {
        (*it)->step();
    }
    
    //Проверяем количество мирных. Спавним, если их недостаточно.
    int counter = 0;
    for(auto it = units.begin(); it != units.end(); it++)
    {
        GetType visitor;
        (*it)->accept(visitor);
        if(visitor.value == "Neutral")
            counter++;
    }
    for(int i = counter; i < minimal_neutrals_amount; i++)
    {
        units.insert(new Neutral(&matrix, &units));
    }
}


void Castle::add_building_in_progress(std::pair<int, int> point, int type)
{
    int x = point.first;
    int y = point.second;
    if(type == 0)
        Control::buildings_in_progress.erase(std::make_pair(x,y));
    else
    {
        if(matrix[x][y] != nullptr)
        {
            GetType visitor;
            matrix[x][y]->accept(visitor);
            if(visitor.value == "Building")
                return;
        }
        
        Control::buildings_in_progress.insert(std::make_pair(x, y));
    }
}
    
    std::vector <std::vector <double> > Castle::get_dots() const
    {
        // 1 - подконтрольный
        // 2 - агрессивный
        // 3 - нейтрал
        // 4 - святилище
        // 5 - строение
        // 6 - запланированное строение
        std::vector <std::vector <double> > result;
        for(auto it = Control::buildings_in_progress.begin(); it != Control::buildings_in_progress.end(); it++)
        {
            std::vector <double> point;
            point.push_back(it->first);
            point.push_back(it->second);
            point.push_back(0);
            point.push_back(6); // идентификатор типа запланированного строения
            result.push_back(point);
        }
        for(int i = 0; i < matrix.size(); i++)
        {
            for(int j = 0; j < matrix[i].size(); j++)
            {
                if(matrix[i][j]==nullptr)
                    continue;
                else
                {
                    std::vector <double> point;
                    GetType visitor;
                    matrix[i][j]->accept(visitor);
                    point.push_back(i); // x
                    point.push_back(j); // y
                    point.push_back(0); // z
                    point.push_back(visitor.priority+1); //идентификатор типа юнита
                    result.push_back(point);
                }
            }
        }
        return result;
    }
    