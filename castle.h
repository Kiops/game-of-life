#pragma once

#include <units_of_castle.h>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <list>


class Castle
{
    Matrix matrix;
    
    std::set <Unit*> units; 
    
    void create_matrix();
    void set_starting_state();
public:
    Castle();
    ~Castle();
    void restart();
    void step();
    void add_building_in_progress(std::pair<int, int> point, int type);
    std::vector <std::vector <double> > get_dots() const; //первые три элемента - координаты x, y, z
    //третий - идентификатор типа юнита
};
