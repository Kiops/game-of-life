 #pragma once

#include <vector>
#include <QObject>
#include <QTimer>
#include "model.h"
#include "view.h"
#include "grand_view.h"
#include "castle.h"

class Controller : public QObject
{
    Q_OBJECT
    
    Model model;
    Castle castle;
    Grand_view window;
    
    
    std::vector <std::vector <std::vector <int>>> history;
    int generation_number;
    bool draw_mode_was_used;
    
    QTimer * timer;

    bool is_castle_mode;
public:
    Controller();
    
    void paint();
    void start();
    
public slots:
    void clear();
    void restart();
    void change_step();
    void change_step(bool forward);
    void place_cell(QPoint, int);
    void play_or_pause(bool);
    void change_mode(bool);
};
