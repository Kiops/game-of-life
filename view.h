#pragma once
#include <QOpenGLWidget>
#include <QMouseEvent>
#include <QWidget>


class View : public QOpenGLWidget
{
    Q_OBJECT
    std::vector <std::vector <double>> points;
    
    QSize size;
    
protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    
    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    
public:
    View(QWidget* parent = 0);
    ~View();
    QSize sizeHint() const;
    
public slots:
    void update(std::vector <std::vector <double>> new_coords);
    
signals:
    void place_cell(QPoint, int);
};
